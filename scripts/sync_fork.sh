#!/bin/bash
set -x

# sync upstream infinity-auto-builder
if [ "SYNC_CI_FORK" != "no" ] && [ "$(git branch --show-current)" = "main" ]; then
	git remote add upstream https://gitlab.com/American_Jesus/infinity-autobuild.git
	git fetch upstream
	git merge upstream/main
fi